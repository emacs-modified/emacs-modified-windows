# Emacs Modified for Windows

**Emacs Modified for Windows** is a distribution of [GNU Emacs](https://www.gnu.org/software/emacs/) bundled with a few select packages for R developers and  LaTeX users, most notably [ESS](https://ess.r-project.org) and [AUCTeX](https://www.gnu.org/software/auctex/). It also comes with a spell checker, additional image libraries and a convenient installation wizard. The distribution is based on the [official releases](http://ftp.gnu.org/gnu/emacs/windows/) of GNU Emacs.

Other than the few selected additions and some minor configuration, this is a stock distribution of Emacs. Users of Emacs on other platforms will appreciate the similar look and feel of the application.

The [official project page](https://emacs-modified.gitlab.io) provides detailed information on the distribution and links to binary releases.

## Repository content

The repository contains a few distribution-specific files and a `Makefile` to fetch the other components and combine everything into an installer based on [Inno Setup](http://innosetup.com). The complete source code of Emacs and the packages is not hosted here.

## Prerequisites

Building the distribution on Windows requires a number of Unix utilities that do not come bundled with the operating system. Therefore, one will need to install the following components:

1. The [MSYS2](http://www.msys2.org) building platform for Windows. This provides a Unix shell and standard utilities.

2. The following additional MSYS2 packages: `make`, `dos2unix`, `texinfo` and `p7zip` (if 7Zip is not otherwise installed on the system). To install, use
   
   ```
   pacman -S make dos2unix texinfo p7zip
   ```
   
   from the MSYS command line.

4. [Inno Setup](http://innosetup.com) to create the installer.

## Building the distribution

Edit the `Makeconf` file to set the version numbers of GNU Emacs, the distribution and the various extensions. Then `make` or `make all` will launch the following three main steps:

1. `get-packages` will fetch the binary release of GNU Emacs and the extensions.

2. `emacs` will, in summary, decompress the GNU binary distribution in a temporary directory, add all the packagess into the application tree and build the installer.

3. `release` will upload the installer to the GitLab package registry and create a release with the a link to the installer in the release notes.

Each of the above three steps is split into smaller recipes, around 20 in total. See the `Makefile` for details.

## Publishing on GitLab

Uploading files and publishing a release on GitLab from the command line involves using the [GitLab API](https://docs.gitlab.com/ee/api/README.html). The interested reader may have a look at the `upload` and `create-release` recipes in the `Makefile` to see how we achieved complete automation of the process, including the extraction of the release notes from the `NEWS` file.

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
